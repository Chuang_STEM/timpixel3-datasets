import sys
sys.path.append(r'C:\Users\DJannis\Documents\TiCorEM\ticorem')

import os
Path = r'Z:\ematcm30\Daen\ScanningTimepix-31-01-2020'
os.chdir(Path)

Path = r'D:\4DSTEM-Superlattice-14-02-2020\test'




import numpy as np
import hyperspy.api as hs
import matplotlib.pyplot as plt
from importlib import reload
import pulse_processor_tools as ppt
import timepix_tools as tt
import time_correlation_tools as tct
import time
import pixstem.api as ps

#name = 'Fri_Jan_31_16_53_59_2020illum_test_scan'
name = 'Fri_Jan_31_17_48_31_2020bf_signal'

timepix_name = name+'_e'

events = tt.open_t3p(timepix_name, chunks = (3,8000000), sort = True)
events = events.compute()

subetime =  events[2,:]

mintime = 4.998e9
maxtime = 5e9

st = events[2,:][(events[2,:]>mintime) & (events[2,:]<maxtime)]
st1 = (st-mintime)/1e3


bins = np.arange(0,2e3, 1)
he = np.histogram(st1, bins)

t_axis = he[1][:-1]
em_e = he[0]

plt.figure()
plt.plot(t_axis, em_e)
plt.xlabel(r'time [$\mu$m]')
plt.ylabel('Counts')

fft, freq = tct.time_to_fourier(t_axis, em_e)

plt.figure()
plt.plot(freq, np.abs(fft))

events_t = np.copy(events)
events_t[2,:] = events[2,:]*1.000115

dwell_time = 40e3
offset = 1.75e9+dwell_time*400
flyback_time = 7654487+25e3
scan_size = (1024,1024)

end_time = offset + scan_size[0]*flyback_time + scan_size[0]*scan_size[1]*dwell_time

image = tt.remake_sum_scan_signal_fast(events_t, scan_size, dwell_time, flyback_time, offset)

fov = (78.2)**2
elec_nm = image[0].sum()/fov


adf = hs.load('17.52.29 Scanning Acquire_1.ser')

plt.figure(figsize=(12,4))
plt.subplot(121)
plt.imshow(image[0], vmax=20)
plt.xticks([])
plt.yticks([])
plt.colorbar()
plt.subplot(122)
plt.imshow(adf.data)
plt.xticks([])
plt.yticks([])
plt.colorbar()
plt.savefig('gold_spheres.png',dpi=700)





plt.figure()
plt.imshow(image_n[::2, ::2]+image_n[1::2, 1::2])

mtime = dwell_time*scan_size[0]*scan_size[1]+scan_size[1]*flyback_time

end_scanline = scan_size[0]*dwell_time
end_line = end_scanline + flyback_time
line_edges = np.arange(0, end_scanline, int(dwell_time))
line_edges = line_edges.astype(np.float64)
line_edges = np.concatenate((line_edges,np.array([end_scanline,end_line])))
t_edges = np.copy(line_edges)    
for i in range(1,scan_size[1]):
    n = line_edges + i*line_edges[-1]
    t_edges = np.concatenate((t_edges,n[1:])) 


H, edges = np.histogram(events[2,:], bins = (t_edges))
H = np.reshape(H, (scan_size[0], scan_size[1]+1))

plt.figure()
plt.imshow(H[:,:-1])

plt.figure()
plt.plot(H[:,:-1].sum(1))




























