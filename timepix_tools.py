import numpy as np
import matplotlib.pyplot as plt
import h5py
from tqdm import tqdm
import dask.array as da
from dask.diagnostics import ProgressBar
from scipy.optimize import curve_fit
import pixstem.api as ps
from numba import jit
import numba



from scipy.io import savemat
import os

@numba.njit
def hist1d(v,b):
    return np.histogram(v, b)[0]

@jit
def histogram1d(samples, nbins):
    '''Return a uniformly binned histogram of samples.
   
    The histogram will span ``hmin`` to ``hmax``, and have a number of
    bins equal to ``nbins``.
    '''
    hist = np.zeros(shape=nbins, dtype=np.int32)
    bin_width = nbins[2] - nbins[1]
    hmin = nbins[0]
    hmax = nbins[-1]
    for element in samples:
        bin_index = int((element - hmin) / bin_width)
        if 0 <= bin_index < nbins:
            hist[bin_index] += 1
        elif element == hmax:
            hist[-1] += 1  # Reproduce behavior of numpy
    return hist


def open_t3p(name, chunks = (3,4000000), sort = True):
    filename = name+".t3p"
    frametype = np.dtype(
            [('index', np.uint32), ('toa', np.uint64), ('overflow', np.uint8),
            ('ftoa', np.uint8), ('tot', np.uint16)])
    data_mem = np.memmap(filename, frametype, mode='r')

    x_pixel = da.from_array(data_mem['index'])//256
    y_pixel = da.from_array(data_mem['index'])%256
#    toa = da.from_array(data_mem['toa'])*25 - 25*da.from_array(data_mem['ftoa'])/16
    toa = da.from_array(data_mem['toa'])*25

    data = [x_pixel, y_pixel, toa]
    #data = [y_pixel, x_pixel, toa]

    dask_array = da.stack(data, axis=0)
    dask_array = dask_array.rechunk(chunks)
    dask_array = dask_array.astype('float64')
    if sort:   
        dask_array = sort_events_dask(dask_array)
    return dask_array





def convert_to_mat_file(events, name, computed = False, shape=(256, 256)):
    name = name + '.mat'
    if computed == False:
        print('The dask_array wasn\'t computed yet. This will happen now, before saving the .mat-file.' )
        events = events.compute()
        xbins = np.arange(shape[0] + 1) - 0.5
        ybins = np.arange(shape[1] + 1) - 0.5

        for i in range(0, xbins+ybins)
            for t in range(0,xbins+ybins)
                for j in range(0, xbins)
                    for k in range(0,ybins)
                        image(i, t, j, k) = events(j, k)

        saved_mat = savemat(os.path.join(os.getcwd(), name) , {'events': image})
    else:
        print('Good job you haven\'t forgotten to compute the dask_array. The .mat-file will now be saved')
        xbins = np.arange(shape[0] + 1) - 0.5
        ybins = np.arange(shape[1] + 1) - 0.5

        for i in range(0, xbins+ybins)
            for t in range(0, xbins+ybins)
                for j in range(0, xbins)
                    for k in range(0,ybins)
                        image(i, t, j, k) = events(j, k)

        saved_mat = savemat(os.path.join(os.getcwd(), name) , {'events': image})
    return saved_mat

#     def sum_image(co, shape=(256, 256)):
#
#         xbins = np.arange(shape[0] + 1) - 0.5
#         ybins = np.arange(shape[1] + 1) - 0.5
#         im = np.histogram2d(co[0, :], co[1, :], bins=[xbins, ybins])
#         im = np.flipud(np.rot90(im[0]))
#         return im



def open_t3p_raw(name, chunks = (4,4000000), sort = True):
    """ Opens the .tp3 singal without converting the time to 
    nanoseconds but leaving it in the interger mode. No ftoa
    is taken into account at this point.
    """
    filename = name+".t3p"
    frametype = np.dtype(
            [('index', np.uint32), ('toa', np.uint64), ('overflow', np.uint8),
            ('ftoa', np.uint8), ('tot', np.uint16)])
    data_mem = np.memmap(filename, frametype, mode='r')


    x_pixel = da.from_array(data_mem['index'])//256
    y_pixel = da.from_array(data_mem['index'])%256
    toa = da.from_array(data_mem['toa'])
    data = [x_pixel, y_pixel, toa]
    dask_array = da.stack(data, axis=0)
    dask_array = dask_array.rechunk(chunks)

    if sort:   
        dask_array = sort_events_dask(dask_array)
    return dask_array


def sort_events_chunks(events):
    co = np.argsort(events[2,:])
    events = events[:,co]
    return events

def sort_events_dask(dask_array, lazy_result = True, show_progressbar = False):
    output = da.map_blocks(sort_events_chunks, dask_array, dtype = np.float64, chunks = dask_array.chunksize)
    if lazy_result:
        return output
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        output = output.compute()
        
        if show_progressbar:
            pbar.unregister()

def _timepix_metadata(name):
    """
    Create the dictionary of the metadata which is
    interesing. In this case acquisition time, dac
    values and threshold value

    Parameters:
        name (string): The filename of the Timepix signal

    Returns:
        metadata (dictionary): The keys and values of the
            metadata.
    """
    metaname = name+".t3p.info"
    with open(metaname, 'r')as file:
       for i,line in enumerate(file):
            if i == 11:
                metadata = dict(Acq_time = line[:-3])
            if i == 19:
                metadata['DACs'] = line[:-3]
            if i == 51:
                metadata['Threshold'] = line[:-3]

    return metadata

def cluster_events(events, dt):
    """Clustering events by taking the sorted time array
    and picking the events which occur in time difference dt.
    Then there wil be checked if the two subsequent events 
    are pixels next to each other. Function can be used for
    the chunks of the dask array.
    Parameters:
        events (numPy array): The dask array of the events
        dt (float): the time difference in which a subsequent
            events should be detected for it to identity with
            the same electron.

    Returns:
        m_events (numPy array): Filtered events.

    """
    co = np.where(np.diff(events[2,:])<= dt)[0]
    if co.size== 0:
        print("no clusters found")
        return events
    dx = events[0, co] - events[0, co+1]
    dy = events[1, co] - events[1, co+1]
    boolean = (np.abs(dx)) <= 1 & (np.abs(dy) <= 1) 
    m_events = np.delete(events, co[boolean]+1, 1)
    return m_events
    
def cluster_events_extra(events, dt):
    """Clustering events with the time difference between events
    Parameters:
        events (numPy array): The dask array of the events
        dt (float): the time difference in which a subsequent
            events should be detected for it to identity with
            the same electron.

    Returns:
        m_events (numPy array): Filtered events.

    """
    time_difference = []
    diff = np.diff(events[2,:])
    co = np.where(diff<= dt)[0]
    if co.size== 0:
        print("no clusters found")
        return events
    dx = events[0, co] - events[0, co+1]
    dy = events[1, co] - events[1, co+1]
    boolean = (np.abs(dx)) <= 1 & (np.abs(dy) <= 1) 
    m_events = np.delete(events, co[boolean]+1, 1)
    time_difference.extend(diff[co[boolean]])
    
    return m_events, time_difference


def cluster_events_dask(events, dt = 100, lazy_result = True, show_progressbar = True):
    """Taking all electron events into account and making the sum image
    for the loaded dask array.
    Parameters:
        events (dask array): The dask array of the events
        lazy_result (bool)
        show_progressbar (bool): Boolean to indicate if progressbar
            (default=True)

    Returns:
        im: (2D numpy array) Electron events on the X and Y coordinates

    """
    kwargs = {'dt': dt}
    output = da.map_blocks(cluster_events, events, dtype = np.float64, chunks = events.chunksize, **kwargs)
    if lazy_result:
        return output
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        output = output.compute()
        
        if show_progressbar:
            pbar.unregister()

    
    return output

def sum_image(co, shape = (256,256)):
    """
    Taking all electron events into account and making the sum image
    Parameters:
        Xco (1D numpy array): X coordinate of events
        Yco (1D numpy array): Y coordinate of events
        shape (tuple): Shape of the reconstructed image

    Returns:
        im: (2D numpy array) Electron events on the X and Y coordinates

    """

    xbins = np.arange(shape[0] + 1) - 0.5    
    ybins = np.arange(shape[1] + 1) - 0.5
    im = np.histogram2d(co[0,:],co[1,:], bins=[xbins, ybins])
    im = np.flipud(np.rot90(im[0]))
    return im


def sum_image_dask(events, shape = (256,256), show_progressbar = True):
    """Taking all electron events into account and making the sum image
    for the loaded dask array.
    Parameters:
        events (dask array): The dask array of the events
        shape (tuple): The shape of the detector (default= (256,256))
        show_progressbar (bool): Boolean to indicate if progressbar
            (default=True)

    Returns:
        im: (2D numpy array) Electron events on the X and Y coordinates

    """
    events_co = events[0:2,:]
    chunks =  (2, events.chunksize[1])
    kwargs = {'shape': shape}
    output = da.map_blocks(sum_image, events_co, dtype = np.float32, chunks = chunks, **kwargs)

    if show_progressbar:
        pbar = ProgressBar()
        pbar.register()
    output = output.compute()
    
    if show_progressbar:
        pbar.unregister()
    
    image = np.zeros(shape)
    for i in range(len(events.chunks[1])):
        k = i*256
        image = image + output[:,k:k+256]
    
    return image

def remove_artifacts(events, binsize, std_num, plotting = False):
    bins = np.arange(0, events[2,:].max(), binsize)
    h = hist1d(events[2,:], bins)
    med = np.median(h)
    std = np.std(h)
    thres = med + std_num*std
    un_t = np.where(h> thres)[0]
    boolean = np.ones(events.shape[1],dtype = bool)
    for co in un_t:
        boolean[(events[2,:] >=bins[co]) & (events[2,:] <=(bins[co+1]))] = False
    rem_events = np.delete(events, np.where(boolean==False)[0], 1)
    f = hist1d(rem_events[2,:], bins)
    if plotting:
        fig, ax = plt.subplots(2,1)
        ax[0].plot(bins[:-1],h)
        ax[0].set_title('uncorrected')
        ax[0].set_xlabel('time [ns]')
        ax[1].plot(bins[:-1],f)
        ax[1].set_title('corrected')
        ax[1].set_xlabel('time [ns]')
        fig.show()
    return rem_events
    
def remove_artifacts_dask(events, binsize=1e6, std_num=5, lazy_result = True, show_progressbar = False):

    kwargs = {'binsize': binsize, 'std_num': std_num}
    chunks =  events.chunksize
    output = da.map_blocks(remove_artifacts, events, dtype = np.float64, chunks = chunks, **kwargs)
    if lazy_result:
        return output
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        output = output.compute()
    return output
    

def scan_remake_4d(events, scan_size, dwell_time, flyback_time, offset, pixels = 256):
    """
    Parameters:
        events (dask array): The dask array of the events
        scan_size (tuple): The shape of the scan
        dwell_time (int): dwell time in ns
        flyback_time (int): flyback time in ns
        offset (float): offset between start detector and start scan in ns

    Returns:


    """
    end_scanline = scan_size[0]*dwell_time
    end_line = end_scanline + flyback_time
    line_edges = np.arange(0, end_scanline, int(dwell_time))
    line_edges = line_edges.astype(np.float64)
    line_edges = np.concatenate((line_edges,np.array([end_scanline,end_line])))
    scan_time = offset + scan_size[0]*flyback_time + scan_size[0]*scan_size[1]*dwell_time
    t_edges = np.copy(line_edges)    
    for i in range(1,scan_size[1]):
        n = line_edges + i*line_edges[-1]
        t_edges = np.concatenate((t_edges,n[1:])) 
    
    t_edges = t_edges + offset
    boolean = (events[2,:] >= offset) & (events[2,:] <= scan_time)
    pix_bins = np.linspace(0, 256, pixels+1)   
    H, edges = np.histogramdd(np.transpose(events[:3,boolean]), bins = (pix_bins, pix_bins, t_edges))
    H = np.reshape(H, (pixels,pixels,scan_size[0], scan_size[1]+1))
    return H, edges, scan_time

def multiple_scan_4d_signal(n_scan, events, scan_size, dwell_time, flyback_time, offset, pixels):
    scan_image = np.zeros((pixels, pixels, scan_size[0], scan_size[1]+1))
    for i in tqdm(range(n_scan)):
        im ,edges, offset = scan_remake_4d(events, scan_size, dwell_time, flyback_time, offset, pixels=pixels)
        scan_image = scan_image + im
    return ps.PixelatedSTEM(scan_image)


def remake_sum_scan_signal_fast(events, scan_size, dwell_time, flyback_time, offset):
    t_edges, scan_time = get_time_bins(scan_size, dwell_time, flyback_time, offset)
    boolean = (events[2,:] >= offset) & (events[2,:] <= scan_time)

    H = hist1d(events[2,boolean], t_edges)
    scan = np.reshape(H, (scan_size[0], scan_size[1]+1)) 

    
    return scan, scan_time


def get_time_bins(scan_size, dwell_time, flyback_time, offset):
    end_scanline = scan_size[0]*dwell_time
    end_line = end_scanline + flyback_time
    line_edges = np.arange(0, end_scanline, int(dwell_time))
    line_edges = line_edges.astype(np.float64)
    line_edges = np.concatenate((line_edges,np.array([end_scanline,end_line])))
    scan_time = offset + scan_size[0]*flyback_time + scan_size[0]*scan_size[1]*dwell_time
    t_edges = np.copy(line_edges)    
    for i in range(1,scan_size[1]):
        n = line_edges + i*line_edges[-1]
        t_edges = np.concatenate((t_edges,n[1:])) 
    t_edges = t_edges + offset
    return t_edges, scan_time


def remake_sum_scan_signal(events, scan_size, dwell_time, flyback_time, offset):
    print('dd')
    scan_image = np.zeros((scan_size[0], scan_size[1]+1))
    start_time = offset
    dwell_line = dwell_time*scan_size[0]
    bins_dwell = np.linspace(0, dwell_line, scan_size[0]+1)
    bins = np.concatenate((bins_dwell, np.array([bins_dwell[-1]])))
    delta_t = bins[-1]
    for j in tqdm(range(scan_size[1])):
        boolean = (events[2,:] >= start_time) & (events[2,:] <= (start_time+delta_t))
        nbins = bins+start_time
        scan_image[j,:] = hist1d(events[2,boolean], nbins)
        start_time = start_time + delta_t + flyback_time
        
    
    return scan_image, start_time


def multiple_scan_sum_signal(n_scan, events, scan_size, dwell_time, flyback_time, offset):
    scan_image = np.zeros((n_scan, scan_size[0], scan_size[1]+1))
    for i in tqdm(range(n_scan)):
        im , offset = remake_sum_scan_signal_fast(events, scan_size, dwell_time, flyback_time, offset)
        scan_image[i,:,:] = im

    return ps.PixelatedSTEM(scan_image)

def bf_scan_signal(events, scan_size, dwell_time, flyback_time, offset):
    H, edge = scan_remake_4d(events, scan_size, dwell_time, flyback_time, offset)
    image = np.sum(H, axis = (0,1))
    return image

def eels_background_func(x,A,r):
    """ Power law function to fit
    """
    return A*x**-r

def eels_background_fit(x, y, fit_bound, edge_bound):
    """ Simple power law fit to calculate the background
    and this returns the number of electrons detected 
    in the edge_bound
    """
    
    subx = x[(x>=fit_bound[0]) & (x<=fit_bound[1])]
    suby = y[(x>=fit_bound[0]) & (x<=fit_bound[1])]
    popt, pcov = curve_fit(eels_background_func, subx, suby)
    backg = eels_background_func(x, popt[0], popt[1])
    f = y - backg
    counts = f[(x>=edge_bound[0]) & (x<=edge_bound[1])].sum()
    
    return counts, backg


def framebyframe_per_period(events, period, maxtime, start_time = 0, binsize = 1e6):
    """ Calculates the frame by frame for the timepix signal
    with a known period, normally the blanking, so we can see
    how fast the signal gets blanked on the GIF side.
    """
    wraps = np.ceil(maxtime/period).astype('uint16')
    xbins = np.arange(256 + 1)   
    ybins = np.arange(256 + 1)
    t_edges = np.arange(0, period, binsize)
    signal = np.zeros((256,256,t_edges.size-1))
    for i in tqdm(range(wraps)):
        startt = i*period + start_time
        stopt = (i+1)*period + start_time
        boolean = (events[2,:] >= startt) & (events[2,:] < stopt)
        t_bins = t_edges + startt
        H = np.histogramdd(np.transpose(events[:3,boolean]), bins = (xbins, ybins, t_bins))
        signal = signal + H[0]
        
        
#    p = ps.PixelatedSTEM(signal).swap_axes(0,1)
    return signal





























