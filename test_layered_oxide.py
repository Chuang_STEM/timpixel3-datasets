import os
import numpy as np
import matplotlib.pyplot as plt
import h5py
from tqdm import tqdm
import dask.array as da
from dask.diagnostics import ProgressBar
from scipy.optimize import curve_fit
import pixstem.api as ps

import timepix_tools as tpt
import time_correlation_tools as tct

#Path= r"Z:\ematcm30\Daen\4DSTEM-Superlattice-14-02-2020"
#os.chdir(Path)
#print(os.getcwd())


scan_size = (512, 512)
dwell_time = 20e3                      # in nanoseconds!!!!
flyback_time = 7654487+25e3
offset = 1.75e9+dwell_time*400
binsize=1e6
std_num=5


name = "Fri_Feb_14_16_00_32_2020512x512_20us_e"

events = tpt.open_t3p(name)




events = events.compute()               # This turns the dask array to a normal numpy array
computed = True                         # Tell that the dask_array is already computed (needed for the convert_to_mat_file-function)


#saved_mat = tpt.convert_to_mat_file(events, 'test', computed)



# subetime = events[2, :]
# bins = np.arange(0,subetime.max(), 1e6)
# he = np.histogram(subetime, bins)
#
# t_axis = he[1][:-1]
# em_e = he[0]
# plt.figure()
# plt.plot(t_axis, em_e)
#
# fft, freq = tct.time_to_fourier(t_axis, em_e)
# plt.figure()
# plt.plot(freq, np.abs(fft))
# plt.title('Fourier transform of signal')
# plt.xlabel('Frequency')
# plt.ylabel('Signal')



# rem_events = tpt.remove_artifacts(events, binsize, std_num, plotting = True)
#
#image = tpt.sum_image(events, shape = (256,256) )
#plt.imshow(image)
#plt.title('Summed image')
#
#remade_image, scan_time = tpt.remake_sum_scan_signal_fast(events, scan_size, dwell_time, flyback_time, offset)
#plt.imshow(remade_image, vmax = 20)
#plt.title('Remade image')


#H, edges, scan_time = tpt.scan_remake_4d(events, scan_size, dwell_time, flyback_time, offset, pixels = 256);
#plt.imshow(H)


