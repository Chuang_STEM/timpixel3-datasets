import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import dask.array as da
from dask.diagnostics import ProgressBar
import numba


@numba.njit
def hist1d(v,b):
    return np.histogram(v, b)[0]

def make_time_signal(e_time, x_time, maxtime, delta_t):
    subxtime =  x_time[x_time < maxtime]
    subetime =  e_time[e_time< maxtime]
    
    bins = np.arange(0,maxtime, delta_t)
    hx = np.histogram(subxtime, bins)
    he = np.histogram(subetime, bins)
    
    t_axis = he[1][:-1]
    em_e = he[0]
    em_x = hx[0]
    return t_axis, em_e, em_x


def time_to_fourier(t_axis, signal):
    """ Calculate the fourier transform of a time signal
    and returns the signal and frequentie axis. The zero
    frequency is situated at the middle.
    """
    
    fourier = np.fft.fftshift(np.fft.fft(signal))
    n = signal.size
    timestep = t_axis[1] - t_axis[0]
    freq = np.fft.fftshift(np.fft.fftfreq(n, d=timestep))
    return fourier, freq

def fourier_to_time(signal, freq, filter_size, minimum):
    """ Calculate time signal but filtered by the filter 
    size which is more than the frequency of one period.
    The mimimum is a threshold to remove unwanted signal
    """
    filtered_signal = np.copy(signal)
    filtered_signal[np.abs(freq) > filter_size] = 0
    filtered_signal[np.abs(filtered_signal)<minimum] = 0
    plt.figure()
    plt.plot(np.abs(filtered_signal))
    time_sig = np.fft.ifft(np.fft.ifftshift(filtered_signal))
    return time_sig

def autocorrelated(time_axis, signal1, signal2, period):
    """ Calculates the autocorrelation of the two signal where they 
    are filtered and it returns only the autocorrelation up to the 
    first period of the shutter signal because more is irrelevant.
    """

    fourier1 = np.fft.fft(signal1)
    fourier2 = np.fft.fft(signal2)
    product = fourier1*np.conj(fourier2)
    cor = np.fft.ifft(product)
    mperiod = period + 2*period
    autocor = np.abs(cor[time_axis < mperiod])
    ntime = time_axis[time_axis < mperiod]
    return autocor, ntime

def filter_array_relevant(freq, period, filter_size):
    """ Only keep frequency around the harmonics of the
    period known of the shutter
    """
    filter_array = np.zeros_like(freq)
    harmon = 1/period
    n_har = np.floor(filter_size/harmon)
    window = harmon*0.02
    har_array = np.arange(-n_har,n_har)*harmon
    for i in range(har_array.size):
        if (har_array[i]==0):
            pass
        else:
            boolean = (freq>(har_array[i] - window/2)) & (freq<(har_array[i] + window/2))
            filter_array[boolean] = 1
      
    return filter_array


def expected_xrays(N_e, n_detector, omega):
    efficiency = n_detector*0.25*0.7/(4*np.pi)
    return N_e*efficiency*omega


def autocorrelation_function(time_axis, signal1, signal2, time_window = None):
    """ Calculates the autocorrelation.
    """
    fourier1 = np.fft.fft(signal1)
    fourier2 = np.fft.fft(signal2)
    product = fourier1*np.conj(fourier2)
    cor = np.abs(np.fft.ifft(product))
    if time_window != None:
        cor = cor[time_axis < time_window]
        time_axis = time_axis[time_axis < time_window]
        
    return cor, time_axis

def period_unwrap_raw(time_events, period, maxtime, start_time = 0, binsize = 1):
    """Function to find the period of the signal applied to the blanker.
    """
    wraps = np.ceil(maxtime/period).astype('uint16')
    t_edges = np.arange(0, period, binsize)
    signal = np.zeros((t_edges.size-1))
    for i in tqdm(range(wraps)):
        startt = i*period + start_time
        stopt = (i+1)*period + start_time
        boolean = (time_events >= startt) & (time_events < stopt)
        t_bins = t_edges + startt
        H = np.histogram(time_events[boolean], bins = (t_bins))
        signal = signal + H[0]
        
    return t_edges[:-1], signal

def cross_period_signal(time_events, period, maxtime, start_t, binsize = 100, plotting = False):
    """ Returns the cross correlation of two different time unwraps of the
    same time stream.
    """
    t_edges, se =period_unwrap_raw(time_events,
                                   period, maxtime, start_time = start_t[0], binsize = binsize)
    t_edges, se_ = period_unwrap_raw(time_events,
                                     period, maxtime, start_time = start_t[1], binsize = binsize)

    cor = autocorrelation_function(t_edges,se, se_)
    
    timedif = t_edges[np.argmax(cor)]
    center = int(np.ceil(t_edges.size/2))
    n_time = np.concatenate((t_edges[center:]-t_edges[-1],t_edges[:center]))
    n_cor = np.concatenate((cor[center:],cor[:center]))
    if plotting == True:
        plt.figure()
        plt.plot(n_time, n_cor)
    timeshift = n_time[np.argmax(n_cor)]
    return timeshift


def find_proporionality_timepix(e_time, x_time, period, maxtime, delta_t, steps, plotting = False):
    const = 1
    for i in tqdm(range(steps.size)):
        ratio_num = steps[i]
        looping = np.arange(-1,2,1)
        cor_array = []
        max_array = np.zeros(looping.size)
        for j in range(looping.size):
            mult = const + looping[j]*ratio_num
#            correlate_events_in_time(e_time, x_time, mult, delta_t, period)
            time_e = e_time * mult
            t_axis, he, hx = make_time_signal(time_e, x_time, maxtime,
                                                  delta_t)
            auto, tc_axis = autocorrelated(t_axis, he, hx, period)
            cor_array.append(auto)
            max_array[j] = np.max(auto)
            
        const = const + looping[np.argmax(max_array)]*ratio_num    
        if plotting == True:
            plt.figure()
            for fig in cor_array:
                plt.plot(tc_axis, fig)
                plt.title(i)
           
    best_cor = cor_array[np.argmax(max_array)]
            
    return const, best_cor, tc_axis





def find_cross_correlation_dask(time_array, event_array, factor, delta_t, time_window):
    kwargs = {'factor': factor, 'delta_t': delta_t, 'time_window': time_window}
    output = da.map_blocks(correlate_events_in_time, time_array, event_array, dtype = np.float64, chunks = time_array.chunksize, **kwargs)

    output = output.compute()

    return output


def find_proporionality_timepix_dask(time_array, event_array, time_window, maxtime, delta_t, steps, const = 1):
    for i in tqdm(range(steps.size)):
        ratio_num = steps[i]
        looping = np.arange(-1,2,1)
        cor_array = []
        max_array = np.zeros(looping.size)
        for j in range(looping.size):
            mult = const + looping[j]*ratio_num
            auto = find_cross_correlation_dask(time_array, event_array, mult, delta_t, time_window)
            cor_array.append(auto)
            max_array[j] = np.max(auto)
            
        const = const + looping[np.argmax(max_array)]*ratio_num    
           
    best_cor = cor_array[np.argmax(max_array)]
            
    return const, best_cor



    

def correlate_events_in_time(time_array, event_array, factor, delta_t, time_window):
    """ Make the electron and x-ray signal into a 1d function
    and cross correlate them with each other, the electron 
    time evens can be multiplied with a factor.
    """
    time_e = np.multiply(time_array[event_array == 0], factor)
    time_x = time_array[event_array == 1]
    maxtime = np.max([time_e.max(), time_x.max()])
    t_axis, he, hx = make_time_signal(time_e, time_x, maxtime,
                                          delta_t)
    auto, tc_axis =  autocorrelation_function(t_axis, he, hx, time_window)
    return auto



            
            
def create_events_array(events, x, chunks = 5000000):
    """ Make one large events list where we have the time of arrival as
    one array and another which keeps track of the kind of radiation,
    so an X-ray or electron. This is implemented in dask to optimize and
    speed up the calculations. Electrons are 0 and X-rays are 1
    Parameters:
        events (numPy array): The electron events
        x (numPy array): The time of arrival of the x rays
    """
    time_array = np.concatenate((events[2,:], x))
    event_array = np.concatenate((np.zeros(events[2,:].size), np.ones(x.size)))
    arg_co = np.argsort(time_array)
    time_array = da.from_array(time_array[arg_co], chunks = chunks)
    chunks = time_array.chunksize
    event_array = da.from_array(event_array[arg_co], chunks = chunks)
    return time_array, event_array
    




def create_events_array_v1(events, x, window):
    """ 
    
    
    
    """
    time_array = np.concatenate((events[2,:], x))
    event_array = np.concatenate((np.zeros(events[2,:].size), np.ones(x.size)))
    arg_co = np.argsort(time_array)
    time_array = da.from_array(time_array[arg_co], chunks = chunks)
    chunks = time_array.chunksize
    event_array = da.from_array(event_array[arg_co], chunks = chunks)
    return time_array, event_array

















    
    
def correlation_finder_dask(time_array, event_array, time_edge, binsize, lazy_result = True, show_progressbar = False):
    kwargs = {'time_edge': time_edge, 'binsize': binsize}
    output = da.map_blocks(correlation_finder, time_array, event_array, dtype = np.float64, chunks = time_array.chunksize, **kwargs)
    if lazy_result:
        return output
    else:
        if show_progressbar:
            pbar = ProgressBar()
            pbar.register()
        output = output.compute()
        bins = np.arange(time_edge[0],time_edge[1], binsize)
        sh = int(output.size/(bins.size-1))
        output = np.reshape(output,(bins.size-1,  sh), order = 'F').sum(1)
        
        if show_progressbar:
            pbar.unregister()
            
    t_axis = bins[:-1]
    return output, t_axis

#@numba.njit
def correlation_finder(time_array, event_array, time_edge, binsize):
    bins = np.arange(time_edge[0],time_edge[1], binsize)
    hist = np.zeros(int(bins.size-1))
    x_rays = time_array[event_array == 1]
    elec = time_array[event_array == 0]
    for i in tqdm(range(len(x_rays))):
        deltat = elec - x_rays[i]
        boolean = (deltat > time_edge[0]) & (deltat < time_edge[1])
        h = hist1d(elec[boolean],bins)
        hist = hist + h 
        
    return hist        
    
    
    
    
    
    
    
    
    




































